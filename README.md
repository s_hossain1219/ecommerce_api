eCommerce **API**
=========

eCommerce API is a step-by-step guide to implementing an RESTful API into your eCommerce  project using the Django Rest Framework, Django Rest Framework JWT, Django CORS Headers, and more.

## Project Overview

1. Implement a RESTful API ([wikipedia](https://en.wikipedia.org/wiki/Representational_state_transfer)) to a pre-existing eCommerce project made in Django.

2. Products & Categories
	- Create Serializers and API Views
	- Product variations API (pricing + name)
	- Implement product photos

3. Querying & Filtering
	- Create a Search Function for the API
	- Enable a Django Filter for further filtering of Search Results (or List Results)

4. User Specific
	- Cart, Checkout, & finalizing Orders
	- JWT (JSON Web Token) Authentication
	- View User-only Orders

5. Required Packages
	- Everything in `src/requirements.txt` of the eCommerce project
	- Django Rest Framework (for API)
	- Django Rest Framework JWT (for Auth)
	- Django CORS Headers (for cross-origin HTTP requests)


#### [API Guide](./api_guide.md)
